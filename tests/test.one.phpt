<?php

namespace App\Model;

$container = require __DIR__.'/../app/bootstrap.php';

use Nette\Configurator;
use Tester\Assert;
use Nette\Database;

$configurator = new Configurator();

$configurator->enableTracy(__DIR__ . '/../log');

// propojení s databází
\Tracy\Debugger::enable(TRUE);
$database = $container->getByType(Database\Context::class);

$model = $container->getByType(TypeModel::class);

$type = [
    'name' => 'Reklamace'
];

// vlozeni typu

$id = $model->insertType($type);

// kontrola


$fromDB = $database->query('SELECT * FROM type WHERE name = "Reklamace"')->fetch();

Assert::true((bool)$fromDB);

// editace typu

$type = [
    'name' => 'Reklamni poutace'
];

$model->updateType($id, $type);

$fromDB = $database->query('SELECT * FROM type WHERE name = "Reklamni poutace"')->fetch();

Assert::true((bool)$fromDB);

// smazani typu

$model->deleteType($id);

$fromDB = $database->query('SELECT * FROM type WHERE name = "Reklamni poutace"')->fetch();

Assert::false((bool)$fromDB);



