<?php

namespace App\Forms;

use App\Model\PostsModel;
use App\Model\TypeModel;
use Exception;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Forms\Controls\TextInput;
use Nette\Object;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\Strings;
use Tracy\Debugger;


class PostsFormFactory extends Object
{
    /** @var PostsModel Model pro příspěvky. */
    private $postsModel;
    /** @var TypeModel Model pro typy */
    private $typeModel;

    /**
     * @param PostsModel $postsModel automaticky injectovaná třída modelu pro příspěvky
     * @param TypeModel $typeModel automaticky injectovaná třída modelu pro typy
     */
    public function injectDependencies(PostsModel $postsModel, TypeModel $typeModel)
    {
        $this->postsModel = $postsModel;
        $this->typeModel = $typeModel;
    }

    /**
     * Třída pro přidání prvků do formuláře (jsou stejné pro add i edit)
     * @param Container $form
     * @param null $args
     */
    protected function addCommonFields(Container &$form, $args = null)
    {
        $allTypes = $this->typeModel->listTypes();
        $types = array();
        foreach ($allTypes as $a) {
            $types[$a->id] = $a->name;
        }

        $form->addSelect('id_type','Typ', $types)
            ->setAttribute('placeholder', '============')
            ->setAttribute('class', 'form-control')
            ->setRequired('Je třeba vybrat typ');

        $form->addText('name', 'Název příspěvku')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Vyplň název příspěvku')
            ->setRequired('Je třeba název příspěvku');

        $form->addText('priority', 'Priorita')
            ->setType('number')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Vyplň prioritu')
            ->addRule(Form::NUMERIC, 'Můžete zadat pouze čísla')
            ->addRule(Form::RANGE, 'Můžete zadat pouze čísla od %d do %d', [1,100])
            ->setRequired('Je třeba vyplnit prioritu');

        $form->addText('price', 'Cena')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Vyplň cenu')
            ->addRule(Form::FLOAT, 'Můžete zadat pouze čísla')
            ->addRule(Form::MIN, 'Můžete zadat pouze cenu od %d', 1)
            ->setAttribute('placeholder', 'Vyplň cenu')
            ->setRequired(FALSE);

        $form->addText('properties', 'Vlastnosti')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Vyplň vlastnosti')
            ->setRequired(FALSE);

        $form->addTextArea('short_text', 'Krátký popis')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Vyplň krátký popis')
            ->setRequired('Je třeba vyplnit krátký popis');

        $form->addTextArea('long_text', 'Dlouhý popis')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Vyplň dlouhý popis')
            ->setRequired('Je třeba vyplnit dlouhý popis');
    }


    /**
     * Vytváří komponentu formuláře pro přidávání nového příspěvku
     * @param null|array $args další argumenty
     * @return Form formulář pro přidávání nového příspěvku
     */
    public function createAddForm($args = null)
    {
        $form = new Form(NULL, 'addForm');
        $form->addProtection('Ochrana');
        $this->addCommonFields($form);
        $form->addSubmit('save', 'Uložit')
            ->setAttribute('class', 'btn btn-block btn-success');
        $form->onSuccess[] = [$this, "newFormSucceeded"];
        return $form;
    }

    /**
     * Vytváří komponentu formuláře pro editaci příspěvku
     * @param null|array $args další argumenty
     * @return Form formulář pro editaci příspěvku
     */
    public function createEditForm($args = null)
    {
        $form = new Form(NULL, 'editForm');
        $form->addProtection('Ochrana');
        $this->addCommonFields($form);
        $form->addSubmit('save', 'Uložit')
            ->setAttribute('class', 'btn btn-block btn-success');
        $form->addHidden('id');
        $form->onSuccess[] = [$this, "editFormSucceeded"];
        return $form;
    }

    /**
     * Zpracování validních dat z formuláře a následného přidání příspěvku.
     * @param Form      $form   formulář
     * @param Arrays $values data
     */
    public function newFormSucceeded(Form $form, $values)
    {
        $values = $form->getValues();
        try {
            $this->postsModel->insertPost($values);
        } catch (Exception $exception) {
            $form->addError($exception);
        }
    }

    /**
     * Zpracování validních dat z formuláře a následné aktualizace příspěvku.
     * @param Form      $form   formulář
     * @param Arrays $values data
     */
    public function editFormSucceeded(Form $form, $values)
    {
        $values = $form->getValues();
        try {
            $id = $values['id'];
            unset($values['id']);
            $this->postsModel->updatePost($id, $values);
        } catch (Exception $exception) {
            Debugger::log($exception);
            $form->addError($exception);
        }
    }
}
