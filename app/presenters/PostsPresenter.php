<?php

namespace App\Presenters;

use App\Forms\PostsFormFactory;
use App\Model\NoDataFound;
use App\Model\PostsModel;
use Nette;


class PostsPresenter extends BasePresenter
{
    /** @var PostsFormFactory - Formulářová továrnička pro správu příspěvků */
    private $postsFactory;

    /** @var PostsModel - model pro management příspěvků*/
    private $postsModel;

    /**
     * @param PostsFormFactory $postsFactory
     * @param PostsModel $postsModel
     */
    public function injectDependencies( PostsFormFactory $postsFactory, PostsModel $postsModel ) {
        $this->postsFactory = $postsFactory;
        $this->postsModel = $postsModel;
    }

    /**
     * Metoda pro vytvoření formuáře pro vložení
     * @return Nette\Application\UI\Form - formulář
     */
    public function createComponentAddForm()
    {
        $form = $this->postsFactory->createAddForm();
        $form->onSuccess[] = function (Nette\Application\UI\Form $form) {
            $this->redirect('Posts:default');
        };
        return $form;
    }

    /**
     * Metoda pro vytvoření formuláře pro editaci
     * @return Nette\Application\UI\Form - formulář
     */
    public function createComponentEditForm()
    {
        $form = $this->postsFactory->createEditForm();
        $form->onSuccess[] = function (Nette\Application\UI\Form $form) {
            $this->redirect('Posts:default');
        };
        return $form;
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDetail($id) {
        $this->template->post = $this->postsModel->getPost($id);
    }


    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDefault() {
        $this->template->posts = $this->postsModel->listPosts();
        if (!isset($this->template->edit))
            $this->template->edit = NULL;
    }

    /**
     * Signál pro smazání příspěvku
     * @param $id
     */
    public function handleDeletePost($id) {
        try {
            $type = $this->postsModel->getPost($id);
            $this->postsModel->deletePost($id);
            $this->flashMessage('Příspěvěk byl úspěšně smazán', 'success');
            $this->redrawControl('flashes');

        } catch  ( NoDataFound $e) {
            $this->flashMessage('Nelze smazat neexistující prvek!', 'danger');
            $this->redrawControl('flashes');
        }
    }

    /**
     * Nastavení editační proměnné (pro modaly)
     * @param $postId
     */
    public function handleSetEdit($postId) {

        if ($postId != 'new') {
            try {
                $post = $this->postsModel->getPost($postId);

                $this->template->edit = $post;

            } catch  ( NoDataFound $e) {
                $this->flashMessage('Nelze provést toto nastavení!', 'danger');
                $this->redrawControl('flashes');
            }
        } else {
            $this->template->edit = 'new';
        }

        $this->redrawControl('modalRemove');
    }

    /**
     * Akce pro editaci
     * @param int $id id příspěvku
     */
    public function actionEdit($id) {
        $form = $this['editForm'];
        $post = $this->postsModel->getPost($id);
        $form->setDefaults($post);
    }
}
